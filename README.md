# Guide d'installation

!!! A NOTER : Il convient d'installer les VM dans cette ordre afin de garantir le résultat attendu !!! 

## 1- VM Windows Server 

1 - Configurez l'interface en réseau privée hôte, conformément au schéma: https://gitlab.com/davidroussat/b2-projet_infra/-/blob/main/Screenshot/interfaces.png

2 - Récupérez le dossier AD/ de git et placez-le sur le bureau de l'utilisateur. Déplacez vous ensuite à l'intérieur

3 - Exécutez le script "roles_installation.ps1"
    A noter: Le serveur redémarrera

4 - Exécutez le script "configuration.ps1"


## 2- VM Firewall Interne

A NOTER: tous les scripts doivent être lancées avec l'utilisateur root

1 - Installez la VM en tenant compte des interfaces réseaux (réf schéma) SAUF pour l'interface enp0s3 qui doit être en NAT 

2 - Récupérez le dossier 'FW Interne/' de ce git et placez-le dans le répertoire de l'utilisateur root. Déplacez vous ensuite à l'intérieur

3 - Exécutez le script "prerequis.sh"

4 - Changez l'interface réseau NAT par une interface "réseau privée hôte" (réf schéma)

5- Exécutez le script "prerequis.sh"


## 3- VM Windows Client 10

1 - Configurez l'interface en réseau privée hôte, conformément au schéma: https://gitlab.com/davidroussat/b2-projet_infra/-/blob/main/Screenshot/interfaces.png

2 - Récupérez le dossier 'Windows client/' de ce git et placez-le sur le bureau de l'utilisateur. Déplacez vous ensuite à l'intérieur

3 - Exécuter le script "Windows_Client.ps1"

## 4- VM Environnement de test (Git)

A NOTER: tous les scripts doivent être lancées avec l'utilisateur root

1 - Récupérez le dossier git/ de ce git et placez-le dans le répertoire de l'utilisateur root. Déplacez vous ensuite à l'intérieur

2 - Exécuter le script "main.sh"

3- Répondez 'y' à toutes les questions conformément à cette photo: https://gitlab.com/davidroussat/b2-projet_infra/-/blob/main/Screenshot/google_authenticator.png

4 - Téléchargez l'application 'google_authenticator' sur votre téléphone et scannez le QRcode suivant: https://gitlab.com/davidroussat/b2-projet_infra/-/blob/main/Screenshot/QRcode.png OU rentrez le 'secret' afficher en dessous


## 5- VM Firewall Externe

A NOTER: tous les scripts doivent être lancées avec l'utilisateur root

1 - Configurez l'interface en réseau privée hôte, conformément au schéma: https://gitlab.com/davidroussat/b2-projet_infra/-/blob/main/Screenshot/interfaces.png

2 - Récupérez le dossier 'FX Externe/' de ce git et placez-le sur le bureau de l'utilisateur root. Déplacez vous ensuite à l'intérieur

3 - Exécutez le script "main.sh"


## 6- VM Docker

A NOTER: tous les scripts doivent être lancées avec l'utilisateur root

1 - Configurez l'interface en réseau privée hôte, conformément au schéma: https://gitlab.com/davidroussat/b2-projet_infra/-/blob/main/Screenshot/interfaces.png

2 - Récupérez le dossier Docker/ de ce git et placez-le sur le bureau de l'utilisateur. Déplacez vous ensuite à l'intérieur

3 - Exécutez le script "main.sh"

## 7- Utilisation

Une fois l'installation terminée, l'administration du Windows Server, de l'environnement de test (git) et de la machine de production s'effectue depuis le Windows client. 

A noter:

- La connexion au Windows Server s'effectue en RDP

- Arnaud est le seul utilisateur autorisé à se connecter à l'environnement de test, en ssh à l'adresse 10.5.0.1 sur le port 3000

- La connexion en ssh sur le serveur de production est disponible uniquement depuis l'environnement de test

- Sur le serveur de production, seul Arnaud peut lancer des commandes docker en utilisant sudo 
