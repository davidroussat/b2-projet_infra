#/bin/bash
# Installation Docker + déploiement de l'application
# Roussat David ~ 25/05/2022


####### packages installation ##

# Docker
yum install -y yum-utils git

yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

systemctl start docker
systemctl enable docker

# Docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose


# Host configuration 
nmcli connection modify enp0s8 ipv4.addresses 10.0.1.10/24 connection.autoconnect yes ipv4.method manual type ethernet ipv4.dns 8.8.8.8

nmcli connection reload 
nmcli connection down enp0s8
nmcli connection up enp0s8



########### Images configurations ##

# IA service
git clone https://github.com/Arnaudboy/IAserver

# Front service
git clone https://github.com/Arnaudboy/devB2
mv devB2 Front

# Back service
git clone https://github.com/DavidRoussat/Projet-Dev
mv Projet-Dev Back

docker-compose up -d
