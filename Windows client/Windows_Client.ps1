# Configuration réseau 
# Roussat David ~ 25/05/2022

####### Network configuration ##

# Ip Static
Get-NetAdapter -Name "Ethernet" | Set-NetIPInterface -DHCP enabled

# Join Domain

Add-computer –domainname "projet.infra"  -restart
