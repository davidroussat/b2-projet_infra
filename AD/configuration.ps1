# Configuration des services
# Roussat David ~ 25/05/2022


####### DC configuration ##

# OU
Import-Module ActiveDirectory
New-ADOrganizationalUnit -Name Groupes -Path "DC=PROJET,DC=INFRA"
New-ADOrganizationalUnit -Name Utilisateurs -Path "DC=PROJET,DC=INFRA"

# Groups
New-ADGroup -GroupScope Global -Name Dev -DisplayName Dev -GroupCategory Security -PassThru -Path "OU=Groupes,DC=projet,DC=infra"

# Users
New-ADUser -Name arnaud -AccountPassword (ConvertTo-SecureString -AsPlainText Azerty123 -Force) -CannotChangePassword $False -ChangePasswordAtLogon $True -PasswordNeverExpires $False -PasswordNotRequired $False -Path "OU=Utilisateurs,DC=projet,DC=infra" -Enabled $True
New-ADUser -Name clement -AccountPassword (ConvertTo-SecureString -AsPlainText Azerty123 -Force) -CannotChangePassword $False -ChangePasswordAtLogon $True -PasswordNeverExpires $False -PasswordNotRequired $False -Path "OU=Utilisateurs,DC=projet,DC=infra" -Enabled $True

# Add arnaud to Dev group
Add-ADGroupMember -Identity Dev -Members arnaud


####### NPS configuration ##

Import-NpsConfiguration -Path ".\NPS_configuration.xml"


####### Remote Desktop configuration ##

Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
