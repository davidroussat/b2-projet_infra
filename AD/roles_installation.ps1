# Installation des rôles
# Roussat David ~ 25/05/2022

# Variables
$ipAd = "10.9.0.10"
$gateway = "10.9.0.1"

####### Network configuration ##

# Ip Static
Get-NetAdapter -Name "Ethernet" | New-NetIPAddress  -IPAddress $ipAd -AddressFamily IPv4 -PrefixLength 24 -DefaultGateway $gateway

# Dns
Get-NetAdapter -Name "Ethernet" | Set-DnsClientServerAddress -ServerAddresses ($ipAd)

####### Roles Installation ##

# DNS
Install-WindowsFeature -Name DNS -IncludeManagementTools

# Radius
Install-WindowsFeature NPAS -IncludeManagementTools

# Domain controller
Import-Module ADDSDeployment
Install-ADDSForest -DomainName "projet.infra" -DatabasePath "C:\Windows\NTDS" -DomainMode "WinThreshold" -DomainNetbiosName "PROJET" -ForestMode "WinThreshold" -InstallDns -LogPath "C:\Windows\NTDS" -SysvolPath "C:\Windows\SYSVOL" -NoRebootOnCompletion:$true

Restart-Computer -Force


