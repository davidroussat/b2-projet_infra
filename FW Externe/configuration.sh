#/bin/bash
# Définition des règles nftables
# Roussat David ~ 25/05/2022

###### Iptables configuration ##

cat ./nftables_rules > /etc/sysconfig/nftables
systemctl restart nftables.service
