#/bin/bash
# Configuration réseau
# Roussat David ~ 25/05/2022


# Inter - FW
nmcli connection modify enp0s3 ipv4.addresses 10.0.15.1/30 ipv4.gateway 10.0.15.2 connection.autoconnect yes ipv4.method manual type ethernet

nmcli connection reload 
nmcli connection down enp0s3 
nmcli connection up enp0s3

# LAN Server
nmcli connection add enp0s8 ipv4.addresses 10.5.0.1/24 connection.autoconnect yes ipv4.method manual type ethernet

nmcli connection reload 
nmcli connection down enp0s8
nmcli connection up enp0s8

# LAN Client
nmcli connection add enp0s9 ipv4.addresses 10.7.0.1/24 connection.autoconnect yes ipv4.method manual type ethernet

nmcli connection reload 
nmcli connection down enp0s9
nmcli connection up enp0s9


# AD
nmcli connection add enp0s10 ipv4.addresses 10.9.0.1/24 connection.autoconnect yes ipv4.method manual type ethernet

nmcli connection reload 
nmcli connection down enp0s10
nmcli connection up enp0s10
