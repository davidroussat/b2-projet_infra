#/bin/bash
# Configuration des services
# Roussat David ~ 25/05/2022

###### Iptables configuration ##

cat ./iptables > /etc/sysconfig/iptables
systemctl restart iptables.service


##### DHCP Configuration ##

cat ./dhcp_config > /etc/dhcpd.conf
systemctl restart dhcpd.service
