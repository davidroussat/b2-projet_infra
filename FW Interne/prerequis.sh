#/bin/bash
# Installation des services
# Roussat David ~ 25/05/2022

####### Services installation ##

# Installation Iptables

systemctl stop firewalld
systemctl disable firewalld
systemctl mask --now firewalld
yum install iptables-services -y

systemctl enable iptables
systemctl start iptables

# Installation dhcpd

yum -y install dhcp-server

systemctl start dhcpd.service
systemctl enable dhcpd.service



